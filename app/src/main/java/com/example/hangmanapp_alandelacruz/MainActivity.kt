package com.example.hangmanapp_alandelacruz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        splashScreen.setKeepOnScreenCondition{ true }
        //Lògica que vulguem executar abans d'obrir el següent activity
        val intent = Intent(this, SecondActivity::class.java)
        startActivity(intent)
        finish()
    }

}