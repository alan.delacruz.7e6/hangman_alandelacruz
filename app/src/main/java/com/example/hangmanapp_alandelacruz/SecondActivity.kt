package com.example.hangmanapp_alandelacruz

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.hangmanapp_alandelacruz.databinding.ActivitySecondBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class SecondActivity : AppCompatActivity() {

    lateinit var bind: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        bind = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(bind.root)

        bind.helpButton.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle("INSTRUCCIONES DEL HANGMAN")
                .setMessage("Usando una fila de guiones, se representa la palabra a adivinar dando el número de letras que tiene la palabra. Si la letra no está en la palabra, saldrá un elemento de la figura de un hombre ahorcado como una marca de conteo. " +
                        "Deberá adivinar la palabra ya que si sale la figura completa del hombre ahorcado, habrá perdido el juego.")
                .setNegativeButton("BACK") {_, _ ->
                }
                .show()
        }

        val difficultybutton = bind.difficultySpinner
        val dificultad = resources.getStringArray(R.array.difficulty_spinner)


    }

    }

